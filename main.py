# chiedi la password di sblocco
import os
import re
import getpass
from pykeepass import PyKeePass

# Generator: finds all "docker-compose.template.yml" in subdirectories from rootdir
def getDockerComposeFiles(rootdir = '.'):
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            if file == "docker-compose.template.yml":
                yield os.path.join(subdir, file)

def main():
    # Dictionary with all services secrets
    subDict = {}

    print("Unlock password database: ")
    dbpass = getpass.getpass(prompt='Password: ', stream=None)

    # Populate dictionary
    with PyKeePass('secrets.kdbx', password=dbpass) as kp:
        for key in kp.entries:
            service   = key.title

            subDict[service+".username"] = key.username
            subDict[service+".password"] = key.password


    # Scan all yml files for placeholders in form of ${service.username} and ${service.password} and apply
    for filename in getDockerComposeFiles('yml'):
            with open(filename, 'r') as f:
                try:
                    applied = re.sub('\$\{(.*?)\}', lambda x: subDict[x.group(1)], f.read())
                except KeyError as e:
                    print(f"Could not patch {filename}, key {e.args[0]} not found")
                    continue
            filename=filename.replace(".template", "")
            print("writing to " + filename)
            with open(filename, 'w') as f:
                f.write(applied)

main()