# pyGOLEMsecrets

(alpha)

Un tool che permette di applicare le password da un database KeePass a una lista di template file per docker compose.

## Come funziona

1. Salvare i docker compose come `docker-compose.template.yml`.
1. Dato un servizio "servizio", sostituire ogni occorrenza di utente e password rispettivamente con `${servizio.utente}` e `${servizio.password}`.
1. Committare il file epurato dalle password.
1. Sul server, lanciare lo script che provvederà alla sostituzione delle password.